<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('txn_no');
            $table->timestamp('txn_date')->nullable();
            $table->integer('app_user_id')->unsigned();
            $table->foreign('app_user_id')->references('id')->on('app_users');
            $table->integer('package_category_id')->unsigned()->nullable();
            $table->foreign('package_category_id')->references('id')->on('package_categories');
            $table->integer('package_id')->unsigned()->nullable();
            $table->foreign('package_id')->references('id')->on('packages');
            $table->string('status')->nullable();
            $table->float('txn_fees')->nullable();
            $table->float('amount')->nullable();
            $table->timestamps();
            $table->index('txn_no');
            $table->unique('txn_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
