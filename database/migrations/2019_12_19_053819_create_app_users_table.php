<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone_no')->unique();
            $table->string('profile_image')->nullable();
            $table->string('address')->nullable();
            $table->tinyInteger('status')->default(1)->comment='1=active, 2=inactive';
            //$table->integer('total_quiz_point')->default(0);
            //$table->integer('total_puzzle_point')->default(0);
            $table->integer('points')->default(0);
            $table->integer('life')->default(0);
            $table->integer('help')->default(0);
            $table->integer('page_qty')->default(0);
            $table->integer('refer_points')->default(0);
            $table->string('refer_code');
            $table->integer('point_setting_id')->unsigned()->default(1);
            $table->string('app_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('phone_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_users');
    }
}
