<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_category_id')->unsigned()->nullable();
            $table->foreign('package_category_id')->references('id')->on('package_categories');
            $table->string('name');
            $table->integer('quantity');
            $table->integer('price');
            $table->tinyInteger('status')->default(1)->comment='1=active, 2=inactive';
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
