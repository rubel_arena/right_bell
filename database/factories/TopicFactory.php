<?php

use Faker\Generator as Faker;

$factory->define(\App\Topic::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->randomElement(['General Knowledge', 'Bangladesh','International','Sports','Islamic','Entertainment']),
        'take_question_limit' => $faker->randomElement(['8','10','12']),
        'status' => 1,
    ];
});
