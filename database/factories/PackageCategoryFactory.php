<?php

use Faker\Generator as Faker;

$factory->define(App\PackageCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Life','Coin','Help', 'Page']),
        'status' => 1
    ];
});
