<?php

use App\Stage;
use Illuminate\Database\Seeder;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            ['name' => '3 By 3', 'row' => 3, 'column' => 3],
            ['name' => '4 By 3', 'row' => 4, 'column' => 3],
            ['name' => '5 By 3', 'row' => 5, 'column' => 3],
            ['name' => '4 By 4', 'row' => 4, 'column' => 4]
        ];

        Stage::insert($collections);
       /* foreach($collections as $data) {
            Stage::create($data)->each(function ($stage) use($data) {
               $stage->levels()->saveMany(factory(App\Level::class,$data['no_of_level'])->make());
            });
        }*/
    }
}
