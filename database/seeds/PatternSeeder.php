<?php

use App\Pattern;
use Illuminate\Database\Seeder;

class PatternSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*3 by 3 = 6 pattern*/
            ['stage_id' => '1','level' => '1', 'blank_pos' => 3, 'move_pos' => 7, 'status' => 1],
            ['stage_id' => '1','level' => '2','blank_pos' => 0, 'move_pos' => 9, 'status' => 1],
            ['stage_id' => '1','level' => '3','blank_pos' => 0, 'move_pos' => 7, 'status' => 1],
            ['stage_id' => '1','level' => '4','blank_pos' => 0, 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '1','level' => '5','blank_pos' => 0, 'move_pos' => 5, 'status' => 1],
            ['stage_id' => '1','level' => '6','blank_pos' => 0, 'move_pos' => 6, 'status' => 1],

            /*3 by 4 = 6 pattern*/
            ['stage_id' => '2','level' => '1','blank_pos' => 3, 'move_pos' => 10, 'status' => 1],
            ['stage_id' => '2','level' => '2','blank_pos' => 8, 'move_pos' => 12, 'status' => 1],
            ['stage_id' => '2','level' => '3','blank_pos' => 0, 'move_pos' => 12, 'status' => 1],
            ['stage_id' => '2','level' => '4','blank_pos' => 0, 'move_pos' => 11, 'status' => 1],
            ['stage_id' => '2','level' => '5','blank_pos' => 0, 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '2','level' => '6','blank_pos' => 0, 'move_pos' => 5, 'status' => 1],

            /*3 by 5 = 9 pattern*/
            ['stage_id' => '3', 'level' => '1', 'blank_pos' => '6,10', 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '3', 'level' => '2', 'blank_pos' => '4,12', 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '3', 'level' => '3', 'blank_pos' => '4,6,10', 'move_pos' => 12, 'status' => 1],
            ['stage_id' => '3', 'level' => '4', 'blank_pos' => '2,7,9', 'move_pos' => 14, 'status' => 1],
            ['stage_id' => '3', 'level' => '5', 'blank_pos' => '1,15', 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '3', 'level' => '6', 'blank_pos' => '3,13', 'move_pos' => 8, 'status' => 1],
            ['stage_id' => '3', 'level' => '7', 'blank_pos' => '1,3,13', 'move_pos' => 15, 'status' => 1],
            ['stage_id' => '3', 'level' => '8', 'blank_pos' => '1,6,10', 'move_pos' => 15, 'status' => 1],
            ['stage_id' => '3', 'level' => '9', 'blank_pos' => '3,4,12', 'move_pos' => 13, 'status' => 1],
        ];

       Pattern::insert($data);
    }
}
