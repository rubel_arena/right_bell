<?php

use App\Puzzle;
use Illuminate\Database\Seeder;

class PuzzlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            /*Stage 1 level(1-7) pattern(1-6)*/
            ['stage_id' => 1, 'level_id' => 1, 'word_1' => 'কাজ-', 'word_2' => 'কাটানো', 'word_3' => '_আশা'],
            ['stage_id' => 1, 'level_id' => 1, 'word_1' => 'তার-', 'word_2' => 'করতে', 'word_3' => '_তুমি'],
            ['stage_id' => 1, 'level_id' => 1, 'word_1' => 'ছেলে-', 'word_2' => 'কেমন', 'word_3' => '_ছেলে'],
            ['stage_id' => 1, 'level_id' => 2, 'word_1' => 'কেমন', 'word_2' => 'আজকে', 'word_3' => 'ছেলে_'],
            ['stage_id' => 1, 'level_id' => 2, 'word_1' => 'কাটানো', 'word_2' => 'করতে', 'word_3' => 'কাজ_'],
            ['stage_id' => 1, 'level_id' => 3, 'word_1' => 'করছি', 'word_2' => 'আজকে', 'word_3' => '_আমি'],
            ['stage_id' => 1, 'level_id' => 3, 'word_1' => 'মরলে', 'word_2' => 'এমন', 'word_3' => '_তাতে'],
            ['stage_id' => 1, 'level_id' => 4, 'word_1' => 'মরলে', 'word_2' => 'এমন', 'word_3' => 'কা_জ'],
            ['stage_id' => 1, 'level_id' => 4, 'word_1' => 'কেমন', 'word_2' => 'আজকে', 'word_3' => 'আ_শা'],
            ['stage_id' => 1, 'level_id' => 5, 'word_1' => 'কেমন', 'word_2' => 'আ_শা', 'word_3' => 'আজকে'],
            ['stage_id' => 1, 'level_id' => 5, 'word_1' => 'করছি', 'word_2' => 'কা_জ', 'word_3' => 'এমন'],
            ['stage_id' => 1, 'level_id' => 6, 'word_1' => 'করছি', 'word_2' => 'কাজ_', 'word_3' => 'এমন'],
            ['stage_id' => 1, 'level_id' => 7, 'word_1' => 'কাজ-', 'word_2' => 'কাটানো', 'word_3' => '_আশা'],

            /*Stage 2 level(1-7) pattern(1-6)*/
            ['stage_id' => 2, 'level_id' => 16, 'word_1' => 'কাজ-', 'word_2' => 'কাটানো', 'word_3' => 'এমন','word_4' => '_আশা'],
            ['stage_id' => 2, 'level_id' => 16, 'word_1' => 'তার-', 'word_2' => 'আজকে', 'word_3' => 'মরলে','word_4' => '_তুমি'],
        ];

        //Puzzle::insert($collections);
        foreach ($collections as $data) {
            Puzzle::create($data);
        }
    }
}
