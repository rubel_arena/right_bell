<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    public function run()
    {
        DB::table((new User)->getTable())->truncate();

        User::insert([
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@rightbell.com',
                'password'       => bcrypt('UQP7X5rX'),
                'role_id'        => 1,
                'remember_token' => '',
            ],
        ]);
    }
}
