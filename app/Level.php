<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = ['stage_id','level','name','no_of_moves','point','level_cost_2','level_cost_3','level_cost_4','status'];

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function puzzles()
    {
        return $this->hasMany(Puzzle::class, 'level_id');
    }
}
