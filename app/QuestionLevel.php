<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionLevel extends Model
{
    protected $fillable = ['title','level_cost','playable_question','playable_time'];

    public function detail()
    {
       return $this->hasOne(QuestionLevelDetail::class,'question_level_id');
    }
}
