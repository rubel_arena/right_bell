<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUser extends Model
{
    use SoftDeletes;
    //protected $appends = ['profile_image'];
    protected $fillable = ['name','phone_no','profile_image', 'status','points','life','help','page_qty','refer_points','refer_code','point_setting_id','app_token'];
    protected $hidden = ['pivot'];

    public function coins()
    {
        return $this->hasMany(Coin::class,'app_user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'app_user_id');
    }

    public function puzzles()
    {
        return $this->belongsToMany(Puzzle::class, 'app_user_puzzle', 'app_user_id', 'puzzle_id');
    }

    public function puzzleSummery()
    {
        return $this->hasMany(AppUserPuzzleSummery::class,'app_user_id');
    }

    public function getProfileImageAttribute($value)
    {
        return !empty($value) ? "http://192.168.10.101:800/right_bell/public/$value" : '';
    }

    public function pointCategory()
    {
        return $this->belongsTo(PointSetting::class,'point_setting_id','id');
    }
}
