<?php

namespace App\Observers;

use App\AppUser;
use App\Package;
use App\PointSetting;
use Illuminate\Support\Facades\DB;

class CoinActionsObserver
{
    public function created($model)
    {
        //$request = \request()->all();
        //$field = $request['types'] == 1 ? 'total_quiz_point' : 'total_puzzle_point';
        $appUser = AppUser::find($model->app_user_id);
        if ($model->is_increase == 1) {
            //$appUser->{$field} += $request['points'];
            $appUser->points += $model->points;
        } else {
           // $appUser->{$field} -= $request['points'];
            $appUser->points -= $model->points;
            //types 6 is used for life increase
            if ($model->types == 6) {
                $category_id = request()->package_category_id;
                $package = Package::where(['package_category_id' => $category_id, 'id' => request()->package_id])->first();
                //dd($package);
                $appUser->life += $package->quantity;
            }
        }
        $point_category_id = $this->findUserPointCategory($appUser->points);
        $appUser->point_setting_id = $point_category_id;
        $appUser->save();
    }

    public function findUserPointCategory($point)
    {
        $category_id = DB::table('point_settings')
            ->select('id')
            ->where('from','<=',$point)
            ->where('to','>=',$point)
            ->first();
        return $category_id->id;
    }
}