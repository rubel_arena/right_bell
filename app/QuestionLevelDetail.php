<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionLevelDetail extends Model
{
    protected $fillable = ['question_level_id','section_a_no_of_question','section_a_no_of_point','section_b_no_of_question','section_b_no_of_point','section_c_no_of_question','section_c_no_of_point'];

    public function level()
    {
        return $this->belongsTo(QuestionLevel::class,'question_level_id','id');
    }
}
