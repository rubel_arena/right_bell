<?php

namespace App\Http\Controllers;

use App\Http\Requests\PointSettingRequest;
use App\PointSetting;
use Illuminate\Http\Request;

class PointSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = PointSetting::orderBy('id','desc')->get();
        return view('point_settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('point_settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PointSettingRequest $request)
    {
        PointSetting::create($request->all());
        return redirect()->route('pointSettings.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PointSetting  $pointSetting
     * @return \Illuminate\Http\Response
     */
    public function show(PointSetting $pointSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PointSetting  $pointSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(PointSetting $pointSetting)
    {
        return view('point_settings.edit', compact('pointSetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PointSetting  $pointSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointSetting $pointSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PointSetting  $pointSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointSetting $pointSetting)
    {
        //
    }
}
