<?php

namespace App\Http\Controllers;

use App\Http\Requests\StageRequest;
use App\Stage;
use App\Topic;
use Illuminate\Http\Request;

class StageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stages = Stage::all();
        return view('stages.index', compact('stages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StageRequest $request)
    {
        $stage = Stage::create($request->all());
        return redirect()->route('stages.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Stage $stage
     * @return \Illuminate\Http\Response
     */
    public function show(Stage $stage)
    {
        return view('stages.show', compact('stage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Stage $stage
     * @return \Illuminate\Http\Response
     */
    public function edit(Stage $stage)
    {
        return view('stages.edit', compact('stage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Stage $stage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stage $stage)
    {
        $stage->update($request->all());
        return redirect()->route('stages.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Stage $stage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stage $stage)
    {
        //
    }
}
