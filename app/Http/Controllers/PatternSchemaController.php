<?php

namespace App\Http\Controllers;

use App\Pattern;
use App\Stage;
use Illuminate\Http\Request;

class PatternSchemaController extends Controller
{
    public function patternSchema(Request $request)
    {
        if ($request->ajax()) {
            $stage_id = $request->stage_id;
            $level_data = explode(':', $request->level);
            $level_id = $level_data[0];
            $level_sr = $level_data[1];
            $stage = Stage::withCount('patterns')->whereId($stage_id)->first();
            $total_pattern = $stage->patterns_count;
            //select pattern id
            $level = ($level_sr <= $total_pattern) ? $level_sr : $this->deductLevel($level_sr, $total_pattern);
            $pattern = Pattern::where(['stage_id' => $stage_id, 'level' => $level])->first();
            $data = view('puzzles.pattern_schema',compact('stage','pattern','level_id'))->render();
            return response()->json(['schema'=>$data]);
        }
    }

    public function deductLevel($level, $no_of_pattern)
    {
        $pattern_level = $level - $no_of_pattern;
        if ($pattern_level > $no_of_pattern) {
          return $this->deductLevel($pattern_level, $no_of_pattern);
        }
        return $pattern_level;
    }
}
