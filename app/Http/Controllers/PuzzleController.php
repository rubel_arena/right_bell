<?php

namespace App\Http\Controllers;

use App\Http\Requests\PuzzleRequest;
use App\Level;
use App\Puzzle;
use App\Stage;
use Illuminate\Http\Request;

class PuzzleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $puzzles = Puzzle::with('stage','level')->orderBy('id', 'desc')->get();
        return view('puzzles.index', compact('puzzles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stages = Stage::get()->pluck('name', 'id')->prepend('- select stage -', '');
        return view('puzzles.create', compact('stages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PuzzleRequest $request)
    {
        //dd($request->all());
        $data = $request->only('stage_id','level_id');
        $data['word_1'] = $request->has('word_1') ? str_replace(' ', '', implode($request->word_1)) : null;
        $data['word_2'] = $request->has('word_2') ? str_replace(' ', '', implode($request->word_2)) : null;
        $data['word_3'] = $request->has('word_3') ? str_replace(' ', '', implode($request->word_3)) : null;
        $data['word_4'] = $request->has('word_4') ? str_replace(' ', '', implode($request->word_4)) : null;
        $data['word_5'] = $request->has('word_5') ? str_replace(' ', '', implode($request->word_5)) : null;
        $data['word_6'] = $request->has('word_6') ? str_replace(' ', '', implode($request->word_6)) : null;
        $data['word_7'] = $request->has('word_7') ? str_replace(' ', '', implode($request->word_7)) : null;
        $puzzle = Puzzle::create($data);
        return redirect()->route('puzzles.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function show(Puzzle $puzzle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function edit(Puzzle $puzzle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Puzzle $puzzle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Puzzle $puzzle)
    {
        $puzzle->forceDelete();
        return redirect()->route('puzzles.index')->with('message', 'Delete successfully!!');
    }
}
