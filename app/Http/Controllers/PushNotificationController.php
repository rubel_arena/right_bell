<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Http\Requests\PushRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PushNotificationController extends Controller
{
    public function push()
    {
        return view('push.form');
    }

    public function sendPush(PushRequest $request)
    {
        /* $notification = AppUser::all();
         foreach ($notification as $key => $value) {
             $this->notification($value->app_token, $request->body);
         }*/
        $this->notification('hello', $request->body);

        return redirect()->back()->with('message', 'Send notification successfully!!');
    }

    public function notification($to, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' => $title,
            'sound' => true,
        ];

        $extraNotificationData = ["message" => $title, "moredata" => 'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => '/topics/' . $to, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyBgLqme4KLwpbBp_kW_H4SODTDLVuk-nDw',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }
}
