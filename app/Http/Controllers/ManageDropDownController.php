<?php

namespace App\Http\Controllers;

use App\Level;
use App\Stage;
use Illuminate\Http\Request;

class ManageDropDownController extends Controller
{
    public function levelListDependOnStage(Request $request)
    {
        if($request->ajax()){
            $stage_id = $request->stage_id;
            $stage = Stage::withCount('patterns')->whereId($stage_id)->first();
            $collections = Level::where('stage_id',$request->stage_id)->get(["id","name","level"]);
            //$total_pattern = $stage->patterns_count;
            $data = view('ajax-select',compact('collections','stage'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}
