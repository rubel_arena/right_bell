<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\AppUserPuzzleSummery;
use App\Http\Requests\GetPuzzleRequest;
use App\Http\Requests\UserPuzzleSummeryRequest;
use App\Stage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PuzzleController extends ApiController
{
    public function getAllPuzzles(GetPuzzleRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $collections = Stage::with(['levels' => function ($query) {
            $query->select(['id','level','stage_id', 'name', 'no_of_moves','point','level_cost_2','level_cost_3','level_cost_4'])->withCount('puzzles');
        }, 'levels.puzzles' => function ($query) {
            $query->select(['id','level_id', 'word_1', 'word_2', 'word_3', 'word_4', 'word_5', 'word_6', 'word_7']);
        }])->get(['id', 'name', 'row', 'column']);
        return response()->json($collections, 200);
    }

    public function getUserPuzzleSummery(UserPuzzleSummeryRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $puzzleSummery = AppUserPuzzleSummery::where('app_user_id', $request->app_user_id)->get(['stage_id', 'level', 'completed']);
        return response()->json($puzzleSummery, 200);
    }

    public function getUserPuzzleStories(Request $request)
    {
        $appUserId = $request->app_user_id;
        $user = AppUser::find($appUserId);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $userStories = $user->puzzles()->select('id','stage_id','level_id')->get();
        return response()->json($userStories, 200);
    }


}
