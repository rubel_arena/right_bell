<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Http\Requests\Api\AppUserLoginRequest;
use App\Http\Requests\Api\AppUserRegistrationRequest;
use App\Http\Requests\Api\SendSmsRequest;
use App\Http\Requests\AppUserDetailsRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AuthController extends ApiController
{
    public function sendSms(SendSmsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $type = $request->type;
        $data['type'] = $type;


        if ($type == 1) {
            //Request Form Login User
            $response = $this->checkAuthentication($request->phone_no);
            if ($response == true) {
                //already register user -> go to OTP screen And Hit Login API
                $this->send($request);
                $data['status'] = 1;

            } else {
                //not register user -> go to login screen
                $data['status'] = 2;
            }

        } else {
            //Request Form Register User
            $response = $this->checkAuthentication($request->phone_no);
            if ($response == true) {
                //already register user -> go To login Screen
                $data['status'] = 1;

            } else {
                //not register user -> go to OTP screen and hit Registration API
                $this->send($request);
                $data['status'] = 2;
            }
        }
        return response()->json($data, 200);
    }

    public function checkAuthentication($phoneNo)
    {
        $user = AppUser::where('phone_no', $phoneNo)->first();
        return (!empty($user)) ? true : false;
    }

    public function send(Request $request)
    {
        $phoneNo = str_replace('+88', '', $request->phone_no);
        $accode = "a100031p";
        $username = "arena";
        $password = "arena@123#";
        $masking = "ARENAPHONE";
        $isUnicode = "0";
        $receiver = $phoneNo;
        $message = $request->message;

        $endpoint = 'http://202.126.123.152:8001/smsapi/sendsms.php';
        $data1 = [
            'accode' => $accode,
            'username' => $username,
            'passsword' => $password,
            'masking' => $masking,
            'isUnicode' => $isUnicode,
            'receiver' => $receiver,
            'message' => $message,
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $endpoint,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SAFE_UPLOAD => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data1),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function appUserRegistration(AppUserRegistrationRequest $request)
    {
        $referCode = 'RB' . $this->generateReferCode();
        //dd($referCode);
        $data = $request->all();
        $data['status'] = 1;
        $data['total_quiz_point'] = 0;
        $data['total_puzzle_point'] = 0;
        $data['life'] = 5;
        $data['page_qty'] = 0;
        $data['refer_code'] = $referCode;
        $user = AppUser::create($data);
        $user->coins()->create([
            'types' => 3,
            'is_increase' => 1,
            'points' => 3000
        ]);
        if ($request->has('refer_code') && !empty($request->refer_code)) {
            $appUser = AppUser::whereReferCode($request->refer_code)->first();
            if (!empty($appUser)) {
                $appUser->refer_points += 1000;
                $appUser->save();
            }
        }

        $userInfo = AppUser::find($user->id);
        return response()->json($userInfo, 200);
    }

    public function appUserLogin(AppUserLoginRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $user = AppUser::where('phone_no', $request->phone_no)->first();
        if (!empty($user)) {
            $user->app_token = $request->app_token;
            if ($user->isDirty()) {
                $user->save();
            }
            $userInfo = AppUser::find($user->id);
            return response()->json($userInfo, 200);
        } else {
            return response()->json(['message' => 'You Are Not Authenticate'], 200);
        }
    }

    function generateReferCode()
    {
        $number = mt_rand(100000, 999999); // better than rand()
        // call the same function if the barcode exists already
        if ($this->referCodeExists($number)) {
            $newNumber = mt_rand(100000, 999999);
            return $this->referCodeExists($newNumber);
        }

        // otherwise, it's valid and can be used
        return $number;
    }


    function referCodeExists($number)
    {
        // for instance, it might look like this in Laravel
        return AppUser::whereReferCode($number)->exists();
    }

    public function appUserDetails(AppUserDetailsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $user = AppUser::find($request->app_user_id);
        return response()->json($user, 200);
    }

    public function appUserProfileUpdate(UserProfileUpdateRequest $request)
    {
        $appUser = AppUser::find($request->app_user_id);
        if ($request->has('profile_image')) {
            if (!empty($appUser->profile_image)) {
                $image_path = public_path() . '/' . $appUser->profile_image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $image = $request->profile_image;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = $appUser->id . '_' . str_random(10) . '.' . 'png';
            //Image::make($image)->resize(60, 60)->save(public_path('uploads/users') . '/' . $imageName);
            Image::make($image)->save(public_path('uploads/users') . '/' . $imageName);
            //user data update
            $appUser->profile_image = 'uploads/users/' . $imageName;
        }
        $appUser->name = $request->name;
        $appUser->address = $request->address;
        $appUser->save();
        return response()->json($appUser, 200);
    }
}
