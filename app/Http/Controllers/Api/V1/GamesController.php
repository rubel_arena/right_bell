<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\AppUserPuzzleSummery;
use App\Coin;
use App\Http\Requests\Api\LevelRequest;
use App\Http\Requests\Api\LifeDecreaseRequest;
use App\Http\Requests\Api\StoreCoinsRequest;
use App\Http\Requests\TopScorerRequest;
use App\Puzzle;
use App\QuestionLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class GamesController extends ApiController
{
    public function storeCoins(StoreCoinsRequest $request)
    {
        //dd($request->all());
        $appUserId = $request->app_user_id;
        $user = AppUser::find($appUserId);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $coin = $user->coins()->create($request->except('app_user_id'));
        if ($request->has('is_completed') && $request->is_completed == 2) { //1 = complete; 2 = not complete
            $userPuzzle = AppUserPuzzleSummery::where(['app_user_id' => $appUserId, 'stage_id' => $request->stage_id, 'level' => $request->level])->first();
            //dd($userPuzzle);
            if (!empty($userPuzzle)){
                //update puzzle summery
                $userPuzzle->completed += 1;
                $userPuzzle->save();
            } else {
                //create puzzle summery
                $user->puzzleSummery()->create([
                    'stage_id' => $request->stage_id,
                    'level' => $request->level,
                    'completed' => 1,
                ]);
            }
            $userStory = DB::table('app_user_puzzle')
                ->where(['app_user_id'=> $request->puzzle_id,'puzzle_id' => $request->app_user_id])
                ->first();
            if (empty($userStory)) {
                $user->puzzles()->attach($request->puzzle_id);
            }
        }
        $appUserInfo = $coin->appUser;
        return response()->json($appUserInfo, 200);
    }

   public function topScorer(TopScorerRequest $request)
    {
        $user_id = $request->app_user_id;
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $allScorers = DB::table('app_users as u')
            ->join(DB::raw('(SELECT app_user_id, SUM(points) as score FROM coins c 
                where types IN(1,2) AND is_increase=1 AND MONTH(c.created_at) = MONTH(CURRENT_DATE())
                AND YEAR(c.created_at) = YEAR(CURRENT_DATE()) GROUP BY app_user_id ORDER BY SUM(points)) as ts'), function ($join) {
            $join->on('ts.app_user_id', '=', 'u.id');
        })
            //->select(['u.id', 'u.name', 'u.profile_image', 'ts.score']) //Select fields you need.
            ->select('u.id','u.name','ts.score',DB::raw("CONCAT('http://192.168.10.101:800/right_bell/public/','',u.profile_image) as profile_image")) //Select fields you need.
            ->orderBy('ts.score', 'desc')
            ->get();

        $position = $allScorers->search(function($score) use ($user_id){
            return $score->id == $user_id;
        });

        $own_position = $allScorers->where('id',$user_id)->first();
        if (empty($own_position)) {
            $own_position = AppUser::select('id', 'name', 'profile_image')->where('id', $user_id)->first();
            $own_position->score = 0;
            $own_position->position = 0;
        } else {
            $own_position->position = $position + 1;
        }
       // dd($own_position);
        return response()->json(['top_scorer' => $allScorers->take(10), 'own_info' => $own_position], 200);
    }

  /*  public function topScorer(TopScorerRequest $request)
    {
        $user_id = $request->app_user_id;
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $allScorers = AppUser::orderBy()->get(['id','name','profile_image','points']);
        $position = $allScorers->search(function($score) use ($user_id){
            return $score->id == $user_id;
        });

        $own_position = $allScorers->where('id',$user_id)->first();
        $own_position->position = $position + 1;
        return response()->json(['top_scorer' => $allScorers->take(10), 'own_info' => $own_position], 200);
    }*/

    public function levels(LevelRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $levels = QuestionLevel::has('detail')->get(['id','title','playable_question','playable_time','level_cost']);
        return response()->json($levels, 200);
    }

    public function lifeDecrease(LifeDecreaseRequest $request)
    {
        $appUserId = $request->app_user_id;
        $user = AppUser::find($appUserId);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        if ($user->life <= 0) {
            return response()->json(['message' => 'Your life is currently not available']);
        }
        $user->life -= 1;
        $user->save();
        return response()->json($user, 200);
    }
}
