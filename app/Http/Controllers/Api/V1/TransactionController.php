<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Coin;
use App\Http\Requests\Api\BalanceTransferRequest;
use App\Http\Requests\TransactionRequest;
use App\Package;
use App\PointSetting;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class TransactionController extends Controller
{
    public function store(TransactionRequest $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $category_id = $request->package_category_id;
        $package = Package::where(['package_category_id' => $category_id, 'id' => $request->package_id])->first();
        if ($category_id == 3) {
            $transaction = Transaction::create($request->all());
            $user->coins()->create([
                'types' => 5,
                'is_increase' => 1,
                'points' => $package->quantity
            ]);
        } else {
            return response()->json(['message' => 'This category not valid for this transaction'], 422);
        }
        return response()->json($transaction->appuser, 200);
    }
   /* public function store(TransactionRequest $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $category_id = $request->package_category_id;
        $package = Package::where(['package_category_id' => $category_id, 'id' => $request->package_id])->first();
        if ($category_id == 1) {
            $update_field = 'help';
        } elseif ($category_id == 2) {
            $update_field = 'page_qty';
        } elseif ($category_id == 3) {
            $update_field = 'points';
        } else {
            $update_field = 'help';
        }
        //$update_field = ($category_id == 1 ? 'help' : $category_id == 2 ? 'page_qty' : $category_id == 3 ? 'points' : 'help');
        $transaction = Transaction::create($request->all());

        if ($category_id == 3) {
            $user->coins()->create([
                'types' => 5,
                'is_increase' => 1,
                'points' => $package->quantity
            ]);
        } else {
            $user->{$update_field} += $package->quantity;
            $user->save();
        }
        return response()->json($transaction->appuser, 200);
    }*/

    public function balanceTransfer(BalanceTransferRequest $request)
    {
        $transfer_from = AppUser::find($request->transfer_from);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($transfer_from)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $transfer_to = AppUser::where('refer_code', $request->transfer_to)->first();
        if (empty($transfer_to)) {
            return response()->json(['message' => 'Invalid Refer Code']);
        }

        if ($transfer_from->points < $request->points) {
            return response()->json(['message' => 'Coin Unavailable']);
        }

        if ($transfer_from->point_setting_id < 2) {
            $status = $transfer_from->pointCategory->title;
            $point = PointSetting::where('id',2)->first();
            $validStatus = $point->title;
            return response()->json(['message' => "Your Current Status is $status. After $validStatus status can valid this transction"]);
        }

        $point = $request->coins;
        //Decrees Coin
        $decreesData['app_user_id'] = $transfer_from->id;
        $decreesData['points'] = $point;
        $decreesData['types'] = 4;
        $decreesData['is_increase'] = 2;
        $transfer_from = $this->storeCoin($decreesData);

        //Increase Coin
        $increaseData['app_user_id'] = $transfer_to->id;
        $increaseData['points'] = $point;
        $increaseData['types'] = 4;
        $increaseData['is_increase'] = 1;
        $transfer_to = $this->storeCoin($increaseData);
        return response()->json(['transaction_from' => $transfer_from, 'transaction_to' => $transfer_to], 200);
    }

    public function storeCoin($data)
    {
        $coin = Coin::create($data);
        return $coin->appUser;
    }
}
