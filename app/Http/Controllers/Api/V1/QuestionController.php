<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\CurrentQuestion;
use App\Http\Requests\CategoryQuestionsRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\QuestionGetRequest;
use App\Question;
use App\QuestionLevel;
use App\QuestionsOption;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class QuestionController extends ApiController
{
    public function getAllCategories(CategoryRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $categories = Topic::whereStatus(1)->orderBy('id','asc')->get(['id','title']);
        return response()->json($categories, 200);
    }

    public function getAllQuestions(QuestionGetRequest $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        //dd(Carbon::parse()->now());
        //$checkTodayQuestion = CurrentQuestion::whereDate('created_at', '=', date('Y-m-d'))->first();
        $checkTodayQuestion = CurrentQuestion::whereDate('created_at', '=', Carbon::today())->first();
        //dd($checkTodayQuestion);
        if (empty($checkTodayQuestion)) {
            //first delete all previouse data
            CurrentQuestion::truncate();
            //insert data
            $topics = Topic::whereStatus(1)->get();
            foreach ($topics as $topic) {
                $questions = $topic->getTopicsWiseQuestion->map->only(['id', 'topic_id', 'question']);
                foreach ($questions as $question) {
                    $data['id'] = $question['id'];
                    $data['topic_id'] = $question['topic_id'];
                    $data['question'] = $question['question'];
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    CurrentQuestion::create($data);
                }
            }
        }
        //return question with options
        $questions = CurrentQuestion::has('options', '>=', 2)->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy('id')->take($request->take)->get(['id', 'question']);
        if (($questions->count()) > 0 && ($request->is_decrease == 1)) {
            $user->page_qty -= 1;
            $user->save();
        }
        $level = QuestionLevel::whereId($request->level_id)->with(['detail'=> function($query){
            $query->select('id','question_level_id','section_a_no_of_question','section_a_no_of_point','section_b_no_of_question','section_b_no_of_point','section_c_no_of_question','section_c_no_of_point');
        }])->first(['id','title','playable_question','playable_time']);
        //dd($level);
        return response()->json([
            'total_pages' => $user->page_qty,
            'questions' => $questions,
            'level' => $level
        ], 200);

    }

    public function generateResult()
    {
        $options = QuestionsOption::all();
        foreach ($options as $option) {
            if ($option->id % 2 == 0) {
                $option->answer = 0;
                $option->save();
            }
        }
        echo 'success';
    }

    public function getRandomQuestions(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $questions = Question::has('options', '>=', 2)->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy(DB::raw('RAND()'))->take($request->take)->get(['id', 'question']);
        return response()->json($questions, 200);
    }

    public function getDailyQuestions(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $checkTodayQuestion = CurrentQuestion::whereDate('created_at', '=', Carbon::today())->first();
        if (empty($checkTodayQuestion)) {
            //first delete all previouse data
            CurrentQuestion::truncate();
            //insert data
            $topics = Topic::whereStatus(1)->get();
            foreach ($topics as $topic) {
                $questions = $topic->getTopicsWiseQuestion->map->only(['id', 'topic_id', 'question']);
                foreach ($questions as $question) {
                    $data['id'] = $question['id'];
                    $data['topic_id'] = $question['topic_id'];
                    $data['question'] = $question['question'];
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    CurrentQuestion::create($data);
                }
            }
        }
        $questions = CurrentQuestion::has('options', '>=', 2)->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy('id')->take($request->take)->get(['id', 'question']);
        return response()->json($questions, 200);
    }

    public function getReadQuestionsDependOnCategory(CategoryQuestionsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $per_page = $request->has('per_page') ? $request->per_page : '15';
        $questions = Question::has('options', '>=', 2)->select('id','topic_id', 'question')->whereTopicId($request->category_id)->with(['correctOptions' => function ($query) {
            $query->select('question_id', 'option');
        }])->paginate($per_page);
        return response()->json($questions, 200);
    }
}
