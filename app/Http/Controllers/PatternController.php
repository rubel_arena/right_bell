<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatternRequest;
use App\Pattern;
use App\Stage;
use Illuminate\Http\Request;

class PatternController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patterns = Pattern::orderBy('id','desc')->get();
        return view('patterns.index', compact('patterns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stages = Stage::get()->pluck('name', 'id')->prepend('Please select', '');
        return view('patterns.create',compact('stages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatternRequest $request)
    {
        $stageLastPattern = Pattern::where('stage_id', $request->stage_id)->orderBy('level', 'desc')->first();
        $level = !empty($stageLastPattern) ? $stageLastPattern->level + 1 : 1;
        $data = $request->all();
        $data['level'] = $level;
        $pattern = Pattern::create($data);
        return redirect()->route('patterns.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pattern  $pattern
     * @return \Illuminate\Http\Response
     */
    public function show(Pattern $pattern)
    {
        return view('patterns.show', compact('pattern'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pattern  $pattern
     * @return \Illuminate\Http\Response
     */
    public function edit(Pattern $pattern)
    {
        $stages = Stage::get()->pluck('name', 'id')->prepend('Please select', '');
        return view('patterns.edit', compact('pattern','stages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pattern  $pattern
     * @return \Illuminate\Http\Response
     */
    public function update(PatternRequest $request, Pattern $pattern)
    {
        $pattern->update($request->all());
        return redirect()->route('patterns.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pattern  $pattern
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pattern $pattern)
    {
        //
    }
}
