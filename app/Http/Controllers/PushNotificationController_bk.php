<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Http\Requests\PushRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PushNotificationController extends Controller
{
    public function push()
    {
        return view('push.form');
    }

    public function sendPush(PushRequest $request)
    {
        /* $notification = AppUser::all();
         foreach ($notification as $key => $value) {
             $this->notification($value->app_token, $request->body);
         }*/
        $this->notification('hello', $request->title,$request->body);

        return redirect()->back()->with('success', 'send notification successfully!!');
    }

    public function notification($to, $title, $body)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

     /*   $notification = [
            'title' => $title,
            'sound' => true,
        ];*/

        //$extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
        $data = [];
        $data['title'] = $title;
        $data['message'] = $body;
        $data['timestamp'] = date('Y-m-d G:i:s');

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => '/topics/' . $to, //single token
            //'notification' => $notification,
            'data' => $data
        ];

        dd(json_encode($fcmNotification));

        $headers = [
            'Authorization: key=AIzaSyBgLqme4KLwpbBp_kW_H4SODTDLVuk-nDw',
            'Content-Type: application/json'
        ];

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        // Execute post
        $result = curl_exec($ch);
        //dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);

        return $result;
    }
}
