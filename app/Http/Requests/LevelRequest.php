<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stage_id' => 'required',
            'name' => 'required|'.Rule::unique('levels')->where(function ($query) {
                return $query->where('stage_id', request()->stage_id);
            }),
            'level_cost_2' => 'required|integer',
            'level_cost_3' => 'required|integer',
            'level_cost_4' => 'required|integer',
            'no_of_moves' => 'required|integer',
            'point' => 'required|integer'
        ];
    }
}
