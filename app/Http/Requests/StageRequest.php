<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->stage->id);
        return [
            'name' => 'required|unique:stages,name',
            'row' => 'required|integer|between:3,7',
            'column' => 'required|integer|between:3,6',
        ];
    }
}
