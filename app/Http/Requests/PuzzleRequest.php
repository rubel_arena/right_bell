<?php

namespace App\Http\Requests;

use App\Stage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PuzzleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $column = '';
        if (!empty($request->stage_id)) {
            $stage = Stage::whereId(request()->stage_id)->first();
            $column = $stage->column;
        }
        return [
            'stage_id' => 'required',
            'level_id' => 'required',
            'word_1'   => "required|array|min:$column",
            "word_1.*"  => "required|string|min:1",
            'word_2'   => "required|array|min:$column",
            "word_2.*"  => "required|string|min:1",
            'word_3'   => "required|array|min:$column",
            "word_3.*"  => "required|string|min:1",
            'word_4'   => "array|min:$column",
            "word_4.*"  => "required_with:word_4|string|min:1",
            'word_5'   => "array|min:$column",
            "word_5.*"  => "required_with:word_4|string|min:1",
            'word_6'   => "array|min:$column",
            "word_6.*"  => "required_with:word_4|string|min:1",
            'word_7'   => "array|min:$column",
            "word_7.*"  => "required_with:word_4|string|min:1",
        ];
    }
}
