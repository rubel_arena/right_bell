@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.patterns.patterns')</h3>
    
    {!! Form::model($pattern, ['method' => 'PUT', 'route' => ['patterns.update', $pattern->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('stage_id', 'Stage*', ['class' => 'control-label']) !!}
                    {!! Form::select('stage_id', $stages, old('stage_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('stage_id'))
                        <p class="help-block">
                            {{ $errors->first('stage_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('blank_pos', 'Blank Position*', ['class' => 'control-label']) !!}
                    {!! Form::text('blank_pos', old('blank_pos'), ['class' => 'form-control', 'placeholder' => 'Enter blank position']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('blank_pos'))
                        <p class="help-block">
                            {{ $errors->first('blank_pos') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('move_pos', 'Moving Position*', ['class' => 'control-label']) !!}
                    {!! Form::number('move_pos', old('move_pos'), ['class' => 'form-control', 'placeholder' => 'Enter blank position']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('move_pos'))
                        <p class="help-block">
                            {{ $errors->first('move_pos') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

