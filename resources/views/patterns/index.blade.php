@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.patterns.patterns')</h3>

    <p>
        <a href="{{ route('patterns.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($patterns) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Stage</th>
                        <th>Pattern</th>
                        <th>Moving Position</th>
                        <th>Blank Position</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($patterns) > 0)
                        @foreach ($patterns as $key => $pattern)
                            <tr data-entry-id="{{ $pattern->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $pattern->stage->name }}</td>
                                <td>{{ $pattern->level }}</td>
                                <td>{{ $pattern->move_pos }}</td>
                                <td>{{ $pattern->blank_pos }}</td>
                                <td>
                                    <a href="{{ route('patterns.show',[$pattern->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('patterns.edit',[$pattern->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['stages.destroy', $pattern->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection