@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.patterns.patterns')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Stage</th>
                            <td>{{ $pattern->stage->name }}</td>
                        </tr>
                        <tr>
                            <th>Pattern</th>
                            <td>{{ $pattern->level }}</td>
                        </tr>
                        <tr>
                            <th>Moving Position</th>
                            <td>{{ $pattern->move_pos }}</td>
                        </tr>
                        <tr>
                            <th>Blank Position</th>
                            <td>{{ $pattern->blank_pos }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('patterns.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop