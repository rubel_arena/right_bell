@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.stages.stages')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['stages.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Stage*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Enter stage']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('row', 'No of Row*', ['class' => 'control-label']) !!}
                    {!! Form::number('row', old('row'), ['class' => 'form-control', 'placeholder' => 'Enter row']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('row'))
                        <p class="help-block">
                            {{ $errors->first('row') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('column', 'No of Column*', ['class' => 'control-label']) !!}
                    {!! Form::number('column', old('column'), ['class' => 'form-control', 'placeholder' => 'Enter column']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('column'))
                        <p class="help-block">
                            {{ $errors->first('column') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

