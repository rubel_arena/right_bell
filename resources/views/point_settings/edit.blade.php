@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.settings.Point Settings')</h3>
    
    {!! Form::model($pointSetting, ['method' => 'PUT', 'route' => ['pointSettings.update', $pointSetting->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('from', 'Rang From*', ['class' => 'control-label']) !!}
                    {!! Form::number('from', old('from'), ['class' => 'form-control', 'placeholder' => 'Enter Coin Range From']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('to', 'Range To*', ['class' => 'control-label']) !!}
                    {!! Form::number('to', old('to'), ['class' => 'form-control', 'placeholder' => 'Enter Coin Range To']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('to'))
                        <p class="help-block">
                            {{ $errors->first('to') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

