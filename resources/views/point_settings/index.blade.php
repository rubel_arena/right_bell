@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.settings.Point Settings')</h3>

    <p>
        <a href="{{ route('pointSettings.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($settings) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Title</th>
                        <th>Range From</th>
                        <th>Range To</th>
                        {{--<th>Action</th>--}}
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($settings) > 0)
                        @foreach ($settings as $key => $setting)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{ $setting->title or null}}</td>
                                <td>{{ $setting->from or null}}</td>
                                <td>{{ $setting->to or null }}</td>
                               {{-- <td>
                                    <a href="{{ route('questionLevel.show',[$level->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('pointSettings.edit',[$setting->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                  {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['questionLevel.destroy', $level->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>--}}
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection