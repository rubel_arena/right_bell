@php
    $counter = 1;
@endphp
<div class="pattern-schema">
    <input type="hidden" name="level_id" value="{{$level_id}}">
    @for ($i = 1; $i <= $stage->row; $i++)
        <div class="row row-centered">
            @for ($j = 1; $j <= $stage->column; $j++)
                @php
                    $read_only = '';
                    $value = '';
                    $mtrx = $counter++;
                    $moving_position = $pattern->move_pos;
                    $blank_position = explode(',',$pattern->blank_pos);
                    //dd($blank_position);
                    if ($moving_position == $mtrx) {
                        $read_only = 'readonly';
                        $value = '_';
                    }
                    if (in_array($mtrx, $blank_position)) {
                        $read_only = 'readonly';
                        $value = '-';
                    }

                    //echo $mtrx;
                @endphp
                <div class="col-xs-2 form-group col-centered">
                    <input type="text" name="word_{{$i}}[]" value="{{ $value }}" {{ $read_only }} class="form-control" id="mt-{{$i}}-{{$j}}">
                </div>
            @endfor
        </div>
    @endfor
</div>