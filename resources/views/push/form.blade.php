@extends('layouts.app')

@section('content')
    {{--@if(\Session::has('message'))
        <p class="alert {{ \Session::get('alert-class', 'alert-info') }}">{{ \Session::get('message') }}</p>
    @endif--}}
    <h3 class="page-title">@lang('quickadmin.push.send_push')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['push.send']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            Send Push
        </div>

        <div class="panel-body">
            {{--<div class="row row-centered">
                <div class="col-xs-6 form-group col-centered">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control ', 'placeholder' => 'Enter Notification Title']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
            </div>--}}
            <div class="row row-centered">
                <div class="col-xs-6 form-group col-centered">
                    {!! Form::label('body', 'Text Here*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('body', old('body'), ['class' => 'form-control ', 'placeholder' => 'Enter Your Text', 'rows'=>'8']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('body'))
                        <p class="help-block">
                            {{ $errors->first('body') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-xs-6 col-centered">
                    <button type="submit" class="btn btn-danger btn-block" id="submit">
                        <i class="fa fa-paper-plane-o"></i>
                        Send
                    </button>
                    {{--{!! Form::submit("Send", ['class' => 'btn btn-danger btn-block', 'id' => 'submit']) !!}--}}
                </div>
            </div>
        </div>
    </div>
@stop

