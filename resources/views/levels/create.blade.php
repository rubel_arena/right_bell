@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.levels.levels')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['levels.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('stage_id', 'Stage*', ['class' => 'control-label']) !!}
                    {!! Form::select('stage_id', $stages, old('stage_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('stage_id'))
                        <p class="help-block">
                            {{ $errors->first('stage_id') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('point', 'Point*', ['class' => 'control-label']) !!}
                    {!! Form::number('point', old('point'), ['class' => 'form-control', 'placeholder' => 'Enter Point']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('point'))
                        <p class="help-block">
                            {{ $errors->first('point') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('no_of_moves', 'No of Moves*', ['class' => 'control-label']) !!}
                    {!! Form::number('no_of_moves', old('no_of_moves'), ['class' => 'form-control', 'placeholder' => 'Enter blank position']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('no_of_moves'))
                        <p class="help-block">
                            {{ $errors->first('no_of_moves') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level_cost_2', 'Level Cost 2*', ['class' => 'control-label']) !!}
                    {!! Form::number('level_cost_2', old('level_cost_2'), ['class' => 'form-control', 'placeholder' => 'Enter Level Cost 2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level_cost_2'))
                        <p class="help-block">
                            {{ $errors->first('level_cost_2') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level_cost_3', 'Level Cost 3*', ['class' => 'control-label']) !!}
                    {!! Form::number('level_cost_3', old('level_cost_3'), ['class' => 'form-control', 'placeholder' => 'Enter Level Cost 3']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level_cost_3'))
                        <p class="help-block">
                            {{ $errors->first('level_cost_3') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level_cost_4', 'Level Cost 4*', ['class' => 'control-label']) !!}
                    {!! Form::number('level_cost_4', old('level_cost_4'), ['class' => 'form-control', 'placeholder' => 'Enter Level Cost 4']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level_cost_4'))
                        <p class="help-block">
                            {{ $errors->first('level_cost_4') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

