<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    //Manage User
    Route::post('register', 'AuthController@appUserRegistration');
    Route::post('login', 'AuthController@appUserLogin');
    Route::post('user-details', 'AuthController@appUserDetails');
    Route::post('profile-update', 'AuthController@appUserProfileUpdate');
    Route::post('send-sms', 'AuthController@sendSms');

    //curl test
    Route::get('curl-test', 'AuthController@curlTest');

    //Manage Dashboard
    Route::post('top-scorer', 'GamesController@topScorer');


    //Manage Games
    Route::post('categories', 'QuestionController@getAllCategories');
    Route::post('questions', 'QuestionController@getAllQuestions');
    Route::post('random_questions', 'QuestionController@getRandomQuestions');
    Route::post('daily_questions', 'QuestionController@getDailyQuestions');
    Route::post('read_questions', 'QuestionController@getReadQuestionsDependOnCategory');
    Route::get('result-generate', 'QuestionController@generateResult');
    Route::post('store-coin', 'GamesController@storeCoins');
    Route::post('question-levels', 'GamesController@levels');


    //Manage Puzzle Games
    Route::post('get-puzzle', 'PuzzleController@getAllPuzzles');
    Route::post('get-user-puzzle-story', 'PuzzleController@getUserPuzzleStories');
    Route::post('get-user-puzzle-summery', 'PuzzleController@getUserPuzzleSummery');

    //Manage Package category and package
    Route::post('package-category', 'PackageController@getAllCategory');
    Route::post('package-category/packages', 'PackageController@getPackageDependOnCategory');

    //Manage Transaction
    Route::post('transaction-store', 'TransactionController@store');

    //Balance Transfer
    Route::post('balance-transfer', 'TransactionController@balanceTransfer');

    //Reduce Life
    Route::post('life-decrease', 'GamesController@lifeDecrease');



});
